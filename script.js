let title = '';
let john_score, larry_score, year_scale = [];
let global_array = [[]];
let player = {};

let month_of_year = {
  1: "Janvier",
  2: "Février",
  3: "Mars",
  4: "Avril",
  5: "Mai",
  6: "Juin",
  7: "Juillet",
  8: "Août",
  9: "Septembre",
  10: "Octobre",
  11: "Novembre",
  12: "Décembre",

  get_month: (number) => {
    return month_of_year[number];
  }
}

// Méthode pour effacer le contenu de la section détail lors de chaque clic
function clearBox(elementID){
  document.getElementById(elementID).innerHTML = "";
}

// Fonction réutilisable pour le style background des bars
backgroundBarRollover = function (element, over, out) {
  element.onmouseover = function() { this.style.backgroundColor = over; }
  element.onmouseout = function() { this.style.backgroundColor = out; }
}

// Method pour la céation d'élément du DOM avec une text node
createElementAndText = function(element, elementCssClass, id, textNodeValue) {
  let newElement = document.createElement(element);
  newElement.className = elementCssClass;
  newElement.id = id; 

  let newText = document.createTextNode(textNodeValue);
  newElement.appendChild(newText);
  return newElement;
}

// Création du main principale main qui contiendra tout les éléments enfants.
let main = document.createElement('main');
main.className = "main";
main.id = "main"
document.body.appendChild(main);

// Création du container pour l'affichage des scores
let display_score = document.createElement('div');
display_score.className = "display_score";
display_score.id = "score";
document.body.appendChild(display_score);

// Variable pour l'affichage du résultat
let display_box = document.getElementById('score');

// Line Break
let linebreak = document.createElement('br');

// Appel http avec la méthode fetch() et non XmlHttpRequest
fetch('http://cdn.55labs.com/demo/api.json')
.then(res => res.json())
.then(response => {
    this.john_score = response.data.DAILY.dataByMember.players.john.points;
    this.larry_score = response.data.DAILY.dataByMember.players.larry.points;
    this.year_scale = response.data.DAILY.dates;
    this.player = response.settings.dictionary; 
    this.title = response.settings.label;

    let fullLength = this.john_score.length + this.larry_score.length;
    let larry_details = this.player.larry.firstname + " " + this.player.larry.lastname;
    let john_details = this.player.john.firstname + " " + this.player.john.lastname;

    // CREATION DE L'ARRAY 2D POUR L'AFFICHAGE PAR SCORES PAR DATES
    global_array[0]= this.john_score;
    global_array[1] = this.larry_score;
    global_array[2] = this.year_scale;


    let title = document.getElementById('top_title');
    title.innerHTML = this.title;
    
    // CREATION DE LA STRUCTURE
    for(let i = 0; i < fullLength; i++) {
      let box = document.createElement('div');
      box.style.display = 'inline-block';
      main.appendChild(box);
    }
    
    /** AFFICHAGE DES JOURS ET DES SCORES CORRESPONDANT **/
    let toAddDates = document.createDocumentFragment();
    for (let d = 0; d <= this.year_scale.length; d++){
      let dates = document.createElement('p');
      dates.id = 'j' + d;
      dates.className = "day_chart";

      if(this.year_scale[d] != 'undefined' && this.year_scale[d] != null){
        let date_day = this.year_scale[d].substr(6);
        dates.appendChild(document.createTextNode(date_day));
        
        /**
         * NOUS DEVRIONS NORMALEMENT BOUCLER SUR LES INDEX MAIS POUR LA SIMPLICITE
         * NOUS UTILISONS LES VALEURS EN DUR VU QUE LA TAILLE DES ARRAYS SONT CONNUES
         */
        let display = document.getElementById('score');
        dates.addEventListener('click', () => {     
          clearBox('score');
          display_box.style.display = 'block';

          document.getElementById('score').innerHTML = global_array[2][d].substr(6) 
                          + " " + month_of_year.get_month(parseInt(global_array[2][d].substr(4, 2))) + " " +  global_array[2][d].substr(0, 4) 
                          + "<br>" + john_details + ": " + global_array[0][d] 
                          + "<br>" + larry_details + ": " + global_array[1][d] + "<br>";
        });
      }
      toAddDates.appendChild(dates);
    }
    
    document.getElementById('main').appendChild(linebreak);
    document.getElementById('main').appendChild(toAddDates);
    
    /** CHART DE LARRY **/
    let larry_bar_height;
    let larry_box = main.querySelectorAll('div:nth-child(even)');
    
    for(let l = 0; l < this.larry_score.length; l++) {
      if(this.larry_score[l] != 'undefined' && this.larry_score[l] != null){
        larry_bar_height = Math.round(this.larry_score[l] / 4);
        larry_box[l].id = this.larry_score[l];
        larry_box[l].className = 'larry_style';
        larry_box[l].style.height = larry_bar_height + 'px';

        larry_box[l].addEventListener('click', function(){
          clearBox('score');
          display_box.style.display = 'block';

          let larry_score_result = document.getElementById('score');
          let larry_score_display = document.createTextNode('Score de ' + larry_details + ': ' + larry_box[l].id);
          larry_score_result.appendChild(larry_score_display);

        });
      } backgroundBarRollover(larry_box[l], 'lightgrey', 'lightblue');
    }

     /** CHART DE JOHN **/
    let john_bar_height;
    let john_box = main.querySelectorAll('div:nth-child(odd)');

    for(let l = 0; l < this.john_score.length; l++) {
      if(this.john_score[l] != 'undefined' && this.john_score[l] != null){
        john_bar_height = Math.round(this.john_score[l] / 4);

        john_box[l].id = this.john_score[l];
        john_point = this.john_score[l];
        john_box[l].className = 'john_style';
        john_box[l].style.height = john_bar_height + 'px';

        john_box[l].addEventListener('click', function(){
          clearBox('score');
          display_box.style.display = 'block';

          let john_score_result = document.getElementById('score');
          let john_score_display = document.createTextNode('Score de ' + john_details + ': ' + john_box[l].id);
          john_score_result.appendChild(john_score_display);

       });
      } backgroundBarRollover(john_box[l], 'lightgrey', 'lightgreen');
    }

}).catch((error) => {
  console.log('There was an error while trying to reach the ressource: ' + error);
});